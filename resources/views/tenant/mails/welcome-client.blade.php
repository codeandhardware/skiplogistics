@component('mail::layout')
    @slot('header')
       
@endslot
<center> 
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center">
                <img border="0" style="display:block; color:#000000; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px; max-width:35% !important; width:35%; height:auto !important;" width="210"  src="https://skiplogistics.com/wp-content/uploads/2020/08/skip_logo_dark.png">
            </td>
        </tr>
    </tbody>
</table>
<table data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%"  style="table-layout: fixed;">
    <tbody>
        <tr>
            <td style="padding:18px 0px 18px 0px; line-height:10px; text-align:inherit;" height="100%" valign="top" bgcolor="" >
                <div>
                    <h2 style="text-align: center"><span
                            style="font-size: 24px; color: #fc9460; font-family: helvetica, sans-serif">Bienvenido</span>
                    </h2>
                    <h2 style="text-align: center"><span
                            style="font-size: 24px; color: #222f3f; font-family: helvetica, sans-serif"><strong>{{ $client->full_name }}</strong></span></h2>
                    <h2 style="text-align: center"><span
                            style="color: #222f3f; font-family: helvetica, sans-serif; font-size: 18px">Eres
                            Nuestro Cliente</span></h2>
                    <h2 style="text-align: center"><span
                            style="font-size: 24px; font-family: helvetica, sans-serif; color: #fc9460"><strong>{{ $box_code }}{{ $client->manual_id_dsp }}</strong></span>
                    </h2>
                    <div></div>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<!-- {{ __('Hello :who welcome to :what', ['who' => $client->full_name, 'what' => $branch->name ], $lang) }}. {{ __('Below, your box information', [], $lang) }}: <br>
{{ __('Box number', [], $lang) }}: <b>{{ $box_code }}{{ $client->manual_id_dsp }}</b> <br><br> -->

 <div style="font-family: inherit; text-align: left"><span
                                            style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; user-select: text; -webkit-user-drag: none; -webkit-tap-highlight-color: transparent; font-style: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: inherit; text-decoration-style: initial; text-decoration-color: initial; font-size: 11pt; font-family: Calibri, Calibri_EmbeddedFont, Calibri_MSFontService, sans-serif; font-kerning: none; line-height: 19.425px; font-variant-ligatures: none; color: #222f3f">Esta
                                            es la dirección a la cual debes dirigir todas tus compras y paquetes,
                                            nosotros nos encargaremos de recibirlas y llevártelas hasta ti.</span><span
                                            style="color: #222f3f"> &nbsp;</span></div> <br>

<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="3px" style="line-height:3px; font-size:3px;"><tbody><tr><td style="padding:0px 0px 3px 0px;" bgcolor="#d9d9d9"></td></tr></tbody></table>
<br>

<table border="0" cellpadding="5" cellspacing="5" align="center" width="100%" height="3px">
<thead>
  <tr>
    <th><strong style="font-size: 18px; color: #222f3f;">Panamá</strong></th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>
    <center>
    <strong style="font-size: 18px; color: #222f3f;">SERVICIO EXPRESS<br></strong>
      {{ $box_code }}{{ $client->manual_id_dsp }} {{ $client->first_name }} {{ $client->last_name }} <br>
      1345nw 98th ct, Unit 2<br>
      Doral, Fl 33172-2780<br>
      United States<br>
      +1 786-3602816<br><br>

      <strong style="font-size: 18px; color: #222f3f;">SERVICIO STANDARD<br></strong>
    {{ $box_code }}{{ $client->manual_id_dsp }}S {{ $client->first_name }} {{ $client->last_name }} <br>
     1345nw 98th ct, Unit 2<br>
     Doral, Fl 33172-2780<br>
      United States<br>
      +1 786-3602816<br><br>
      
      <strong style="font-size: 18px; color: #222f3f;">SERVICIO MARITIMO<br></strong>
     {{ $box_code }}{{ $client->manual_id_dsp }} {{ $client->first_name }} {{ $client->last_name }} <br>
     6941nw 42nd st<br>
      Miami, Fl 33166<br>
      United States<br>
      +1 305-5997924<br><br>
      </center>
      <div style="font-family: inherit; text-align: justify">
      <h2 style="text-align: center"><span style="color: #fc9460">Importante</span></h2>
      <small><b>{{ $box_code }}{{ $client->manual_id_dsp }}</b> seria tu número de cliente, nunca debe faltar en tus compras, debes agregar {{ $box_code }}{{ $client->manual_id_dsp }} por tu número de cliente, agregar "S" despues de tu número de cliente, ayudara a identificar qué tipo de servicio quieres, es importante saber que si no agregas "S" despues de {{ $box_code }}{{ $client->manual_id_dsp }} tu carga será facturada con la tarifa de servicio AEREO - EXPRESS.</small></div>
    </td>
<!-- 
    <td>
    <center>
    <strong style="font-size: 18px; color: #222f3f;">SERVICIO AEREO<br></strong>
     SKIPLOGISTICS #{{ $client->manual_id_dsp }}<br>
      8001nw 68th St<br>
      Miami, FL 33166<br>
      United States<br>
      + 17867558195<br><br>
      
      <strong style="font-size: 18px; color: #222f3f;">SERVICIO MARITIMO<br></strong>
      SKIPLOGISTICS SEA #{{ $client->manual_id_dsp }}<br>
      8001nw 68th St<br>
      Miami, FL 33166<br>
      United States<br>
      + 17867558195<br><br></center>
      <div style="font-family: inherit; text-align: justify">
      <h2 style="text-align: center"><span style="color: #fc9460">Importante</span></h2><small><b>#{{ $client->manual_id_dsp }}</b> seria tu número de cliente, nunca
      debe faltar en tus compras.
      Debes colocar #{{ $client->manual_id_dsp }} por tu número de cliente, 
      agregar la palabra "SEA" antes de tu número de cliente,
      ayudara a identificar qué tipo de servicio quieres.
      es importante saber que si no agregas
      "SEA" antes de #{{ $client->manual_id_dsp }} tu carga será facturada con la tarifa de servicio EXPRESS.</small></div>
    </td> -->
  </tr>
</tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="3px" style="line-height:3px; font-size:3px;"><tbody><tr><td style="padding:0px 0px 3px 0px;" bgcolor="#d9d9d9"></td></tr></tbody></table>


<table data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" >
                                <tbody>
                                  <tr>
                                    <td style="line-height:15px; text-align:inherit;" height="100%" valign="top" bgcolor="">
                                      <div>
                                        <h2 style="text-align: center"><span style="color: #fc9460"> <br> Importante</span></h2>
                                        <div style="font-family: inherit; text-align: justify"><small>Por favor lea determinadamente la información proporcionada arriba, verifique que los datos usados corresponden al su país de residencia, ya que ofrecemos un servicio de recepción de paquete, no de envió, una vez el paquete llega a su destino, en caso de retorno o garantía, los gastos y demás correrían por cuenta de usted, el cliente.
<br><br>Tratamos de ofrecer un servicio eficiente y correcto, cumpliendo los tiempos estimados dependiendo el servicio que usted haya contratado, no podemos corregir errores con elementos naturales, pandemias y otros.
<br><br>Puede visitar nuestro sitio web www.skiplogistics.com elegir país de preferencia e informarse de tarifas, notificaciones, detalles y otros. Así como también puede usar cualquiera de los otros medios de comunicación que ofrecemos.
<br><br>No nos hacemos responsables por danos, carga abierta o paquetes con contenido no acordado a su compra, en estos días es muy común la cantidad de carga que viene en algunos casos abierta, golpeada, o con su contenido totalmente diferente al comprado. 
<br><br>Todos los paquetes al ser recibidos en bodega de miami son pasados por una inspección de no venir golpeados, abiertos, o con algún detalle, los mismos son nuevamente sellados y embalados, para su viaje correcto a destino.
<br><br>Los clientes en Venezuela, deben entender que ciertas situaciones se sobresalen de nuestras manos, tratamos siempre de entregar su carga de manera segura y eficiente, tratando de evitar malos ratos, malhechores o piratas que puedan atentar contra la vida de nuestros empleados y la integridad de su carga.
<br><br>Es nuestra obligación cumplir con todos los requisitos y requerimientos y cuando trabajamos en conjuntos con nuestros clientes es mucho mas seguro para nosotros la fluidez de trabajo de manera correcta.
<br><br>Skip Logistics Co. Ciudad de Panamá
</small></div>
                                        <div></div>
                                      </div>
                                    </td>
                                  </tr>
                                </tbody>
                              </table><br>

<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="3px" style="line-height:3px; font-size:3px;"><tbody><tr><td style="padding:0px 0px 3px 0px;" bgcolor="#d9d9d9"></td></tr></tbody></table>

<div>
<center> 
<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%"
    style="table-layout: fixed;">
    <tbody>
        <tr>
            <td style="padding:10px 0px 2px 0px; line-height:22px; text-align:inherit;" height="100%" valign="top"
                bgcolor="" role="module-content">
                <div>
                    <div style="font-family: inherit; text-align: center"><span style="font-size: 18px"><strong>Enlaces
                                Importantes</strong></span></div>
                    <div></div>
                </div>
            </td>
        </tr>
    </tbody>
</table>
</center>   
                              <table border="0" cellpadding="0" cellspacing="0" align="center" style="padding:0px 0px 10px 0px;" bgcolor="#FFFFFF">
                                <tbody>
                                  <tr role="module-content">
                                    <td height="100%" valign="top">
                                      <table style="border-spacing:0; border-collapse:collapse; margin:0px 10px 0px 0px;"
                                        cellpadding="0" cellspacing="2" align="left" border="0" bgcolor=""
                                        class="column column-0">
                                        <tbody>
                                          <tr>
                                            <td style="padding:0px;margin:0px;border-spacing:0;">
                                              <table border="0" cellpadding="0" cellspacing="0" class="module">
                                                <tbody>
                                                  <tr>
                                                    <td align="center" bgcolor="" class="outer-td"
                                                      style="padding:0px 0px 0px 0px;">
                                                      <table border="0" cellpadding="0" cellspacing="2"
                                                        class="wrapper-mobile" style="text-align:center;">
                                                        <tbody>
                                                          <tr>
                                                            <td align="center" bgcolor="#222f3f" class="inner-td"
                                                              style="border-radius:6px; font-size:16px; text-align:center; background-color:inherit;">
                                                              <a href="https://box.skiplogistics.com/es/tracking"
                                                                style="background-color:#222f3f; border:1px solid #222f3f; border-color:#222f3f; border-radius:2px; border-width:1px; color:#ffffff; display:inline-block; font-size:14px; font-weight:bold; letter-spacing:0px; line-height:normal; padding:12px 18px 12px 18px; text-align:center; text-decoration:none; border-style:solid; font-family:inherit;"
                                                                target="_blank">Seguimiento De Carga</a>
                                                            </td>
                                                            <td></td>
                                                            <td align="center" bgcolor="#222f3f" class="inner-td"
                                                              style="border-radius:6px; font-size:16px; text-align:center; background-color:inherit;">
                                                              <a href="https://box.skiplogistics.com/es/malidentificados"
                                                                style="background-color:#222f3f; border:1px solid #222f3f; border-color:#222f3f; border-radius:2px; border-width:1px; color:#ffffff; display:inline-block; font-size:14px; font-weight:bold; letter-spacing:0px; line-height:normal; padding:12px 18px 12px 18px; text-align:center; text-decoration:none; border-style:solid; font-family:inherit;"
                                                                target="_blank">Paquetes Extraviados</a>
                                                            </td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                  
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
</div>

<table class="module" role="module" data-type="text" border="0" cellpadding="0"
                                cellspacing="0" width="100%" style="table-layout: fixed;"
                                data-muid="4417dcc9-02a3-4be4-829c-ce9faa1e72d0">
                                <tbody>
                                  <tr>
                                    <td style="padding:18px 0px 18px 0px; line-height:12px; text-align:inherit;"
                                      height="100%" valign="top" bgcolor="" role="module-content">
                                      <div>
                                        <div style="font-family: inherit; text-align: center"><span
                                            style="font-size: 12px; color: #8c8c8c">Tel Panamá: +507 399-2533 | Whatsapp: +507 6011-6491 / + 1 786 7558195
                                            | Telegram: t.me/skiplogistics | www.skiplogistics.com</span></div>
                                        <div style="font-family: inherit; text-align: center"><span
                                            style="font-size: 12px; color: #8c8c8c">© Copyright 2020 |
                                            Skip Logistics Co.</span></div>
                                        <div></div>
                                      </div>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>


@isset($subcopy)
    @slot('subcopy')
        @component('mail::subcopy')
        @endcomponent
    @endslot
@endisset

@slot('footer')

@endslot

@endcomponent